import 'package:flutter/material.dart';
import 'View/Register.dart';
import 'View/Login.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Comic Sans MS',
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Color.fromARGB(255, 174, 231, 255),
      ),
      title: "PetFinder Mobile",
      home: LoginView(),
    );
  }
}
