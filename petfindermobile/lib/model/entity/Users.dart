class UserEntity {
  late String? email;
  late String? lastname;
  late String? name;
  late String? password;

  UserEntity({
    this.name,
    this.lastname,
    this.email,
    this.password,
  });
}
