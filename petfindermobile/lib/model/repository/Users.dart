import '../entity/Users.dart';

class UserRepository {
  final _users = <String, UserEntity>{};

  UserRepository() {
    _users["dgonzalez@hotmail.com"] = UserEntity(
        email: "dgonzalez@hotmail.com",
        name: "Daniel ",
        lastname: "Gonzalez",
        password: "soporte123");

    _users["andres@gmail.com"] = UserEntity(
        /* _users: base de datos */
        email: "andres@gmail.com",
        name: "Andres ",
        lastname: "Urrutia",
        password: "soporte123");
  }
  UserEntity findByEmail(String email) {
    var user = _users[email];

    if (user == null) {
      throw Exception("Usuario no encontrado");
    }

    return user;
  }
}
