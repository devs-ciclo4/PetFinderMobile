import 'package:flutter/material.dart';
import '../Widgets/drawer.dart';

class IndexPage extends StatelessWidget {
  final String email;
  final String name;

  const IndexPage({super.key, required this.email, required this.name});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Index en construccion"),
      ),
      drawer: DrawerWidget(email: email, name: name),
    );
  }
}
