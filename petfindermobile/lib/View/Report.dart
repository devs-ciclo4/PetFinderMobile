import 'package:flutter/material.dart';

import '../Widgets/drawer.dart';

class ReportPage extends StatelessWidget {
  const ReportPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reporte de mascota perdida"),
      ),
      drawer: const DrawerWidget(email: "", name: ""),
    );
  }
}
