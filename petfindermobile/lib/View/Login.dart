import 'package:flutter/material.dart';
import '../../controller/login.dart';
import '../../controller/Request/LoginRequest.dart';
import 'Index.dart';
import 'Register.dart';

class LoginView extends StatelessWidget {
  final _imageUrl = "Img/logosinf.png";
  late LoginController _controller;
  late LoginRequest _request;

  LoginView({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PetFinder"),

        automaticallyImplyLeading: false,
        // leading: Container(),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _logo(),
              _formulario(context),
              _inicioAlternativo(),
              ElevatedButton.icon(
                label: const Text("Registrarse"),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const RegisterView(),
                      ));
                },
                icon: const Icon(Icons.app_registration),
                style: ElevatedButton.styleFrom(
                    primary: Color.fromARGB(255, 27, 152, 75),
                    textStyle:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        Image.asset(
          _imageUrl,
          height: 200,
          width: 250,
        ),
        const SizedBox(
          height: 30,
        ),
      ],
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _campoCorreoElectronico(),
          const SizedBox(height: 8),
          _campoClave(),
          const SizedBox(height: 20),
          ElevatedButton.icon(
            label: const Text("Iniciar sesión"),
            icon: const Icon(Icons.verified_user),
            style: ElevatedButton.styleFrom(
                primary: Color.fromARGB(255, 43, 121, 167),
                textStyle:
                    TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
            onPressed: () {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();

                // Validar correo y clave en BD
                try {
                  var name = _controller.validateEmailPassword(_request);

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => IndexPage(
                        email: _request.email,
                        name: name,
                      ),
                    ),
                  );
                } catch (e) {
                  // showDialog(
                  //   context: context,
                  //   builder: (context) => AlertDialog(
                  //     title: const Text("Ventas"),
                  //     content: Text(e.toString()),
                  //   ),
                  // );

                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(e.toString())));
                }
              }
            },
          ),
          const SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  Widget _campoCorreoElectronico() {
    return TextFormField(
      maxLength: 50,
      decoration: const InputDecoration(
          contentPadding:
              EdgeInsets.only(top: 20), // add padding to adjust text
          isDense: true,
          hintText: "Correo electrónico",
          prefixIcon: Padding(
            padding: EdgeInsets.only(top: 15), // add padding to adjust icon
            child: Icon(Icons.email),
          )),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El correo electronico es obligatorio";
        }
        if (!value.contains("@") || !value.contains(".")) {
          return "El correo tiene un formato inválido";
        }
        return null;
      },
      onSaved: (value) {
        _request.email = value!;
      },
    );
  }

  Widget _campoClave() {
    return TextFormField(
      maxLength: 30,
      obscureText: true,
      decoration: const InputDecoration(
          contentPadding:
              EdgeInsets.only(top: 20), // add padding to adjust text
          isDense: true,
          hintText: "Contraseña",
          prefixIcon: Padding(
            padding: EdgeInsets.only(top: 15), // add padding to adjust icon
            child: Icon(Icons.security),
          )),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La contraseña es obligatoria";
        }
        if (value.length < 6) {
          return "Minimo debe contener 6 caracteres";
        }
        return null;
      },
      onSaved: (value) {
        _request.password = value!;
      },
    );
  }

  Widget _inicioAlternativo() {
    return Column(
      children: [
        const Text(
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 18,
                fontFamily: 'Comic sans MS'),
            "Inicia sesión con"),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton.icon(
                icon: const Icon(Icons.facebook),
                label: const Text("Facebook"),
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    primary: Color.fromARGB(255, 20, 52, 87),
                    textStyle:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.bold))),
            ElevatedButton.icon(
              label: const Text("Google"),
              onPressed: () {},
              icon: const Icon(Icons.g_mobiledata),
              style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 164, 32, 32),
                  textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
