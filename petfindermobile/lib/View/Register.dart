import 'package:flutter/material.dart';

import 'Login.dart';

class RegisterView extends StatelessWidget {
  final _imageUrl = "Img/logosinf.png";

  const RegisterView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => LoginView()),
              );
            },
            child: const Icon(
              Icons.login, // add custom icons also
            ),
          ),
          title: const Text("Registro de Usuarios"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
              child: Column(children: [
            _logo(),
            const TextField(
              style: TextStyle(color: Color.fromARGB(118, 0, 0, 0)),
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.person_4,
                    color: Color.fromARGB(76, 8, 8, 8),
                  ),
                  hintText: "Nombres",
                  hintStyle: TextStyle(color: Color.fromARGB(118, 0, 0, 0))),
            ),
            const SizedBox(
              height: 25,
            ),
            const TextField(
              style: TextStyle(color: Color.fromARGB(118, 0, 0, 0)),
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.people_alt,
                    color: Color.fromARGB(76, 8, 8, 8),
                  ),
                  hintText: "Apellidos",
                  hintStyle: TextStyle(color: Color.fromARGB(118, 0, 0, 0))),
            ),
            const SizedBox(
              height: 25,
            ),
            const TextField(
              style: TextStyle(color: Color.fromARGB(118, 0, 0, 0)),
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.email,
                    color: Color.fromARGB(76, 8, 8, 8),
                  ),
                  hintText: "Correo electrónico",
                  hintStyle: TextStyle(color: Color.fromARGB(118, 0, 0, 0))),
            ),
            const SizedBox(
              height: 25,
            ),
            const TextField(
              obscureText: true,
              style: TextStyle(color: Color.fromARGB(118, 0, 0, 0)),
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.password,
                    color: Color.fromARGB(76, 8, 8, 8),
                  ),
                  hintText: "Contraseña",
                  hintStyle: TextStyle(color: Color.fromARGB(118, 0, 0, 0))),
            ),
            const SizedBox(
              height: 25,
            ),
            const TextField(
              obscureText: true,
              style: TextStyle(color: Color.fromARGB(118, 0, 0, 0)),
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.password,
                    color: Color.fromARGB(76, 8, 8, 8),
                  ),
                  hintText: "Confirme su contraseña",
                  hintStyle: TextStyle(color: Color.fromARGB(118, 0, 0, 0))),
            ),
            const SizedBox(
              height: 25,
            ),
            ElevatedButton.icon(
              label: const Text("Registrarse"),
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterView(),
                    ));
              },
              icon: const Icon(Icons.app_registration),
              style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 27, 152, 75),
                  textStyle: const TextStyle(
                      fontSize: 12, fontWeight: FontWeight.bold)),
            ),
          ])),
        ));
  }

  Widget _logo() {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        Image.asset(
          _imageUrl,
          height: 200,
          width: 250,
        ),
        const SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
