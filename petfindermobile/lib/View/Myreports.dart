import 'package:flutter/material.dart';

import '../Widgets/drawer.dart';

class MyreportsPage extends StatelessWidget {
  const MyreportsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mis mascotas reportadas"),
      ),
      drawer: const DrawerWidget(email: "", name: ""),
    );
  }
}
